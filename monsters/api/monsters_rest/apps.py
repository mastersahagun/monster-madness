from django.apps import AppConfig


class MonstersRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'monsters_rest'
