from django.contrib import admin
from .models import Monster, MonsterAlignment, MonsterCategory, MonsterLanguage


admin.site.register(Monster)
admin.site.register(MonsterAlignment)
admin.site.register(MonsterLanguage)
admin.site.register(MonsterCategory)
