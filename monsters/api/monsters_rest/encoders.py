from common.json import ModelEncoder

from .models import (Monster, MonsterCategory, MonsterLanguage,
                     MonsterAlignment, MonsterSize)


class AlignmentEncoder(ModelEncoder):
    model = MonsterAlignment
    properties = [
        "name",
        "id",
        "description",
    ]


class LanguageEncoder(ModelEncoder):
    model = MonsterLanguage
    properties = [
        "name",
        "id",
        "description",
    ]


class CategoryEncoder(ModelEncoder):
    model = MonsterCategory
    properties = [
        "name",
        "id",
        "description",
    ]


class SizeEncoder(ModelEncoder):
    model = MonsterSize
    properties = [
        "name",
        "id",
        "description",
    ]


class MonsterEncoder(ModelEncoder):
    model = Monster
    properties = [
        "name",
        "armor_class",
        "hit_point",
        "speed",
        "stat_str",
        "stat_dex",
        "stat_int",
        "stat_wis",
        "stat_cha",
        "skills",
        "senses",
        "challenge",
        "challenge_xp",
        "proficiency_bonus",
        "description",
        "action",
        "language",
        "alignment",
        "category",
    ]

    encoders = {
        "language": LanguageEncoder(),
        "alignment": AlignmentEncoder(),
        "category": CategoryEncoder(),
    }
