from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import (Monster, MonsterLanguage, MonsterCategory, MonsterAlignment, MonsterSize)
from .encoders import (
    MonsterEncoder, LanguageEncoder, AlignmentEncoder, CategoryEncoder,
    SizeEncoder
)


@require_http_methods(["GET", "POST"])
def api_monsters(request):
    if request.method == "GET":
        monsters = Monster.objects.all()
        return JsonResponse(
            {"monsters": monsters},
            encoder=MonsterEncoder
        )
    else:
        content = json.loads(request.body)
        monsters = Monster.objects.create(**content)
        return JsonResponse(
            monsters,
            encoder=MonsterEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_monster(request, pk):
    if request.method == "GET":
        monster = Monster.objects.get(id=pk)
        return JsonResponse(
            monster,
            encoder=MonsterEncoder,
            safe=False
        )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            monster = Monster.objects.get(id=pk)

            props = [
                "name",
                "armor_class",
                "hit_points",
                "speed",
                "stat_str",
                "stat_dex",
                "stat_con",
                "stat_int",
                "stat_wis",
                "stat_cha",
                "skills",
                "senses",
                "challenge",
                "challenge_xp",
                "proficiency_bonus",
                "description",
                "action"
            ]
            for prop in props:
                if prop in content:
                    setattr(monster, prop, content[prop])
            monster.save()
            return JsonResponse(
                monster,
                encoder=MonsterEncoder,
                safe=False
            )
        except Monster.DoesNotExist:
            return JsonResponse({"message": "Monster does not exist"})
    else:
        try:
            monster = Monster.objects.filter(id=pk)
            monster.delete()
            return JsonResponse(
                monster,
                encoder=MonsterEncoder,
                safe=False
            )
        except Monster.DoesNotExist:
            return JsonResponse({"message": "Monster does not exist"})


@require_http_methods(["GET", "POST"])
def api_languages(request):
    if request.method == "GET":
        languages = MonsterLanguage.objects.all()
        return JsonResponse(
            {"languages": languages},
            encoder=LanguageEncoder
        )
    else:
        content = json.loads(request.body)
        languages = MonsterLanguage.objects.create(**content)
        return JsonResponse(
            languages,
            encoder=LanguageEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_language(request, pk):
    if request.method == "GET":
        language = MonsterLanguage.objects.get(id=pk)
        return JsonResponse(
            language,
            encoder=LanguageEncoder,
            safe=False
        )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            language = MonsterLanguage.objects.get(id=pk)

            props = [
                "name",
                "description",
            ]
            for prop in props:
                if prop in content:
                    setattr(language, prop, content[prop])
            language.save()
            return JsonResponse(
                language,
                encoder=LanguageEncoder,
                safe=False
            )
        except Monster.DoesNotExist:
            return JsonResponse({"message": "Monster language does not exist"})
    else:
        try:
            language = MonsterLanguage.objects.filter(id=pk)
            language.delete()
            return JsonResponse(
                language,
                encoder=LanguageEncoder,
                safe=False
            )
        except MonsterLanguage.DoesNotExist:
            return JsonResponse({"message": "Monster Language does not exist"})


@require_http_methods(["GET", "POST"])
def api_alignments(request):
    if request.method == "GET":
        alignments = MonsterAlignment.objects.all()
        return JsonResponse(
            {"alignments": alignments},
            encoder=AlignmentEncoder
        )
    else:
        content = json.loads(request.body)
        alignments = MonsterAlignment.objects.create(**content)
        return JsonResponse(
            alignments,
            encoder=AlignmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_alignment(request, pk):
    if request.method == "GET":
        alignment = MonsterAlignment.objects.get(id=pk)
        return JsonResponse(
            alignment,
            encoder=AlignmentEncoder,
            safe=False
        )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            alignment = MonsterAlignment.objects.get(id=pk)

            props = [
                "name",
                "description",
            ]
            for prop in props:
                if prop in content:
                    setattr(alignment, prop, content[prop])
            alignment.save()
            return JsonResponse(
                alignment,
                encoder=AlignmentEncoder,
                safe=False
            )
        except MonsterAlignment.DoesNotExist:
            return JsonResponse({
                "message": "Monster alignment does not exist",
                })
    else:
        try:
            alignment = MonsterAlignment.objects.filter(id=pk)
            alignment.delete()
            return JsonResponse(
                alignment,
                encoder=AlignmentEncoder,
                safe=False
            )
        except MonsterAlignment.DoesNotExist:
            return JsonResponse({
                "message": "Monster alignment does not exist",
                })


@require_http_methods(["GET", "POST"])
def api_categories(request):
    if request.method == "GET":
        categories = MonsterCategory.objects.all()
        return JsonResponse(
            {"categories": categories},
            encoder=CategoryEncoder
        )
    else:
        content = json.loads(request.body)
        categories = MonsterCategory.objects.create(**content)
        return JsonResponse(
            categories,
            encoder=CategoryEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_category(request, pk):
    if request.method == "GET":
        category = MonsterCategory.objects.get(id=pk)
        return JsonResponse(
            category,
            encoder=CategoryEncoder,
            safe=False
        )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            category = MonsterCategory.objects.get(id=pk)

            props = [
                "name",
                "description",
            ]
            for prop in props:
                if prop in content:
                    setattr(category, prop, content[prop])
            category.save()
            return JsonResponse(
                category,
                encoder=CategoryEncoder,
                safe=False
            )
        except MonsterCategory.DoesNotExist:
            return JsonResponse({"message": "Monster Category does not exist"})
    else:
        try:
            category = MonsterCategory.objects.filter(id=pk)
            category.delete()
            return JsonResponse(
                category,
                encoder=CategoryEncoder,
                safe=False
            )
        except MonsterCategory.DoesNotExist:
            return JsonResponse({"message": "Monster Category does not exist"})


@require_http_methods(["GET", "POST"])
def api_sizes(request):
    if request.method == "GET":
        sizes = MonsterSize.objects.all()
        return JsonResponse(
            {"sizes": sizes},
            encoder=SizeEncoder
        )
    else:
        content = json.loads(request.body)
        sizes = MonsterSize.objects.create(**content)
        return JsonResponse(
            sizes,
            encoder=SizeEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_size(request, pk):
    if request.method == "GET":
        size = MonsterSize.objects.get(id=pk)
        return JsonResponse(
            size,
            encoder=SizeEncoder,
            safe=False
        )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            size = MonsterSize.objects.get(id=pk)

            props = [
                "name",
                "description",
            ]
            for prop in props:
                if prop in content:
                    setattr(size, prop, content[prop])
            size.save()
            return JsonResponse(
                size,
                encoder=SizeEncoder,
                safe=False
            )
        except MonsterSize.DoesNotExist:
            return JsonResponse({"message": "Monster size does not exist"})
    else:
        try:
            size = MonsterSize.objects.filter(id=pk)
            size.delete()
            return JsonResponse(
                size,
                encoder=SizeEncoder,
                safe=False,
            )
        except MonsterSize.DoesNotExist:
            return JsonResponse({"message": "Monster size does not exist"})
