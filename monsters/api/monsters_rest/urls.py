from django.urls import path

from .views import (
    api_monsters,
    api_monster,
    api_languages,
    api_language,
    api_alignments,
    api_alignment,
    api_categories,
    api_category,
    api_sizes,
    api_size

)

urlpatterns = [
    path(
        "monsters/",
        api_monsters,
        name="api_monsters",
    ),
    path(
        "monsters/<int:pk>/",
        api_monster,
        name="api_monster",
    ),
    path(
        "languages/",
        api_languages,
        name="api_languages",
    ),
    path(
        "languages/<int:pk>/",
        api_language,
        name="api_language",
    ),
    path(
        "alignments/",
        api_alignments,
        name="api_alignments",
    ),
    path(
        "alignments/<int:pk>/",
        api_alignment,
        name="api_alignment",
    ),
    path(
        "categories/",
        api_categories,
        name="api_categories",
    ),
    path(
        "categories/<int:pk>/",
        api_category,
        name="api_category",
    ),
    path(
        "sizes/",
        api_sizes,
        name="api_sizes",
    ),
    path(
        "sizes/<int:pk>/",
        api_size,
        name="api_size",
    ),
]
