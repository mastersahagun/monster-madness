from django.db import models


class MonsterCategory(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField()


class MonsterAlignment(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField()


class MonsterLanguage(models.Model):
    name = models.CharField(max_length=20)
    description = models.TextField()


class MonsterSize(models.Model):
    name = models.CharField(max_length=10)
    description = models.TextField()


class Monster(models.Model):
    name = models.CharField(max_length=50)
    picture_url = models.URLField()
    armor_class = models.IntegerField()
    hit_points = models.IntegerField()
    speed = models.IntegerField()
    stat_str = models.PositiveSmallIntegerField()
    stat_dex = models.PositiveSmallIntegerField()
    stat_con = models.PositiveSmallIntegerField()
    stat_int = models.PositiveSmallIntegerField()
    stat_wis = models.PositiveSmallIntegerField()
    stat_cha = models.PositiveSmallIntegerField()
    skills = models.TextField()
    senses = models.TextField()
    challenge = models.PositiveSmallIntegerField()
    challenge_xp = models.PositiveIntegerField()
    proficiency_bonus = models.PositiveSmallIntegerField()
    description = models.TextField()
    action = models.TextField()

    category = models.ForeignKey(
        MonsterCategory,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    alignment = models.ForeignKey(
        MonsterAlignment,
        related_name="alignments",
        on_delete=models.CASCADE,
    )

    languages = models.ForeignKey(
        MonsterLanguage,
        related_name="languages",
        on_delete=models.CASCADE,
    )

    sizes = models.ForeignKey(
        MonsterSize,
        related_name="sizes",
        on_delete=models.CASCADE,
    )
