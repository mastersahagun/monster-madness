import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import MonsterForm from './Monsterform';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/monsters/new" element={<MonsterForm />} />
          {/* <Route path="/" element={<ListMonsters />} /> */}
          {/* <Route path="/" element={<MainPage />} /> */}
          {/* <Route path="/" element={<MainPage />} /> */}
          {/* <Route path="/" element={<MainPage />} /> */}
          {/* <Route path="/" element={<MainPage />} /> */}
          {/* <Route path="/" element={<MainPage />} /> */}
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
