import React, { useEffect, useState } from 'react';

function MonsterForm() {
    const [languages, setLanguages] = useState([])
    const [alignments, setAlignments] = useState([])
    const [categories, setCategories] = useState([])
    const [sizes, setSizes] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        armor_class: '',
        hit_points: '',
        speed: '',
        stat_str: '',
        stat_dex: '',
        stat_con: '',
        stat_int: '',
        stat_wis: '',
        stat_cha: '',
        skills: '',
        senses: '',
        challenge: '',
        challenge_xp: '',
        proficiency_bonus: '',
        description: '',
        action: '',


    })

    const fetchLanguageData = async () => {
        const url = `http://localhost:8100/api/languages/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLanguages(data.languages);
        }
    }


    const fetchAlignmentData = async () => {
        const url = `http://localhost:8100/api/alignments/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAlignments(data.alignments);
        }
    }


    const fetchCategoriesData = async () => {
        const url = `http://localhost:8100/api/categories/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCategories(data.categories);
        }
    }

    const fetchSizeData = async () => {
        const url = `http://localhost:8100/api/sizes/`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSizes(data.sizes);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8100/api/monsters/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                armor_class: '',
                hit_points: '',
                speed: '',
                stat_str: '',
                stat_dex: '',
                stat_con: '',
                stat_int: '',
                stat_wis: '',
                stat_cha: '',
                skills: '',
                senses: '',
                challenge: '',
                challenge_xp: '',
                proficiency_bonus: '',
                description: '',
                action: '',

            });
            event.target.reset();
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    useEffect(() => {
        fetchLanguageData();
        fetchAlignmentData();
        fetchCategoriesData();
        fetchSizeData();
    }, []);

    console.log(formData)
    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a Monster</h1>
                <form onSubmit={handleSubmit} id="create-monster-form">
                <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="name" required type="text" id="name" name="name" className="form-control"/>
                        <label htmlFor="name">Monster Name</label>
                    </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="armor_class" required type="number" id="armor_class" name="armor_class" className="form-control"/>
                    <label htmlFor="name">Armor Class</label>
                </div>
                <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="hit_points" required type="number" id="hit_points" name="hit_points" className="form-control"/>
                        <label htmlFor="hit_points">Health</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="speed" required type="number" id="speed" name="speed" className="form-control"/>
                    <label htmlFor="speed">Speed</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="stat_str" required type="number" id="speed" name="stat_str" className="form-control"/>
                    <label htmlFor="stat_str">Str</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="stat_dex" required type="number" id="stat_dex" name="stat_dex" className="form-control"/>
                    <label htmlFor="stat_dex">Dex</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="stat_con" required type="number" id="stat_con" name="stat_con" className="form-control"/>
                    <label htmlFor="stat_con">Con</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="stat_int" required type="number" id="stat_int" name="stat_int" className="form-control"/>
                    <label htmlFor="stat_int">Int</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="stat_wis" required type="number" id="stat_wis" name="stat_wis" className="form-control"/>
                    <label htmlFor="stat_wis">Wis</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="stat_cha" required type="number" id="stat_cha" name="stat_cha" className="form-control"/>
                    <label htmlFor="stat_cha">Cha</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="skills" required type="text" id="skills" name="skills" className="form-control"/>
                    <label htmlFor="skills">Skills</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="senses" required type="text" id="senses" name="senses" className="form-control"/>
                    <label htmlFor="senses">Senses</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="challenge" required type="number" id="challenge" name="challenge" className="form-control"/>
                    <label htmlFor="challenge">Challenge</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="challenge_xp" required type="text" id="challenge_xp" name="challenge_xp" className="form-control"/>
                    <label htmlFor="challenge_xp">Challenge XP</label>
                </div>
                <div className="form-floating mb-3">
                    <input onChange={handleFormChange} placeholder="proficiency_bonus" required type="number" id="proficiency_bonus" name="proficiency_bonus" className="form-control"/>
                    <label htmlFor="proficiency_bonus">Proficiency Bonus</label>
                </div>
                <div className="form-floating mb-3">
                    <textarea onChange={handleFormChange} placeholder="description" required id="description" rows="3" name="description" className="form-control"/>
                    <label htmlFor="description">Description</label>
                </div>
                <div className="form-floating mb-3">
                    <textarea onChange={handleFormChange} placeholder="action" required id="action" rows="3" name="action" className="form-control"/>
                    <label htmlFor="action">Action</label>
                </div>


                <div className="mb-3">
                    <select onChange={handleFormChange} required id="language" name="language" className="form-select">
                        <option>Choose a Language</option>
                        {languages.map(language => {
                return (
                    <option key={language.id} value={language.id}>{language.name}</option>
                )
                })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleFormChange} required id="categories" name="categories" className="form-select">
                        <option>Choose a Category</option>
                        {categories.map(category => {
                return (
                    <option key={category.id} value={category.id}>{category.name}</option>
                )
                })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleFormChange} required id="size" name="size" className="form-select">
                        <option>Choose a Size</option>
                        {sizes.map(size => {
                return (
                    <option key={size.id} value={size.id}>{size.name}</option>
                )
                })}
                    </select>
                </div>
                <div className="mb-3">
                    <select onChange={handleFormChange} required id="alignment" name="alignment" className="form-select">
                        <option>Choose a Alignment</option>
                        {alignments.map(alignment => {
                return (
                    <option key={alignment.id} value={alignment.name}>{alignment.name}</option>
                )
                })}
                    </select>
                </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default MonsterForm;
