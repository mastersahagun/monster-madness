import React, { useEffect, useState } from "react";

function ListMonsters (){

    const [monsters, setMonsters] = useState([])
    async function listMonsters() {
        const response = await fetch("http://localhost:8100/api/monsters/");
        if (response.ok) {
            const data = await response.json();
            setMonsters(data);
        }
    }

    useEffect(() => {
        listMonsters();
    }, []);

    const handleDelete = {async () => {
        const url = `http://localhost:8100/api/monsters/${id}`;
        const response = await fetch(url, {
            method: "delete",
            headers: { "Content-Type": "application/json" },
        });

        if (response.ok) {
            window.location.reload();
        }
        }}

    return(
        <div className="px-4 py-5 my-5 mt-0">
            <table className="table">
            <thead>
            <tr>
            <th>Picture</th>
            <th>Name</th>
            <th>Armor Class</th>

        </tr>
        </thead>
        <tbody>
            <tr>
            <td></td>
            </tr>
        </tbody>
        </table>

            </div>
    );

}



export default ListMonsters
